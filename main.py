##
## File writing functions
##

def writeSvgHeader(f):
	"""writeSvgHeader
	This function writes the default tags for a svg file, \
	plus the <style> tag'
	"""

	f.write(
		'<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n'
		'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 250">\n'
		'\t<style type="text/css">\n'
		'\t\ttext { font-family: DejaVu Sans;\n'
		'\t\t\ttext-anchor: middle;\n'
		'\t\t\talignement-baseline: central;\n'
		'\t\t}'
		'\t\ttext.key-text  { font-size: 64; }\n'
		'\t\ttext.inBetween { font-size: 72; }\n'
		'\t\trect { fill: #C4C4C4; stroke: #A6A6A6; stroke-width: 1.5; }\n'
		'\t</style>\n'
	)

# end of writeSvgHeader()


def writeSvgKey(f, keyName, offset, width):
	"""writeSvgKey
	Creates, in file 'f', a SVG rectangle element positionned at {x: 'offset', \
	y: 100} and containing the text 'keyName'.
	"""

	# For now, use single tab indentation
	indent = '\t'

	# Write to the given file
	f.write(
		'{tab}<g transform="translate({2:d}, 0)">\n'
		'{tab}\t<rect id="key1" width="{0:d}" height="100" rx="20" ry="20" />\n'
		'{tab}\t<text class="key-text" x="{1:d}" y="75">{txt}</text>\n'
		'{tab}</g>\n'
		.format(width, width/2, offset, tab = indent, txt = keyName)
	)

# end of writeSvgKey()


def writeSvgPlusSym(f, pos):
	# define indentation as one tab
	indent = '\t'

	# Write a '+'
	f.write(
		'{tab}<text class="inBetween" x="{0}" y="75">+</text>\n' \
		.format(pos, tab = indent) \
	)

# end of writeSvgPlusSym()


def writeSvgFooter(f):
	"""writeSvgFooter
	This function writes the footer and SVG closing tag in file given as 'f'
	"""

	f.write('</svg>\n')

# end of writeSvgFooter()



##
## Keys string parsing function
##

def parseKeysString(text):
	"""parseKeysString
	This function takes a string of the form '{<key1>|<key2>|...|<keyN>}' and \
	checks its validity.
	Then, an array containing the (properly formatted) text to be displayed and \
	the width on the image is returned
	"""

	# Define lists for some special keys
	tinyKeys   = ['Fn']
	smallKeys  = ['End', 'Del', 'Esc', 'Alt']
	mediumKeys = ['Ctrl', 'Tab']
	largeKeys  = ['Shift', 'Space']
	xlargeKeys = ['Return']

	# Remove any extra spaces from the text
	text = text.strip()

	# Check that the keys string is encolsed in "{}"
	if text.startswith('{') and text.endswith('}'):
		# Remove the surrounding "{}"
		text = text[1:-1]
	else:
		# if not, return error.
		raise SyntaxError('keyComb-gen: String format error')


	# Split the string on '|' character
	keyList = text.split('|')

	outputList = []

	for key in keyList:
		# Capitalize the string (First character uppercase
		#  and everything else lower case)
		key = key.capitalize()

		# If the key is in one of the lists above, set the corresponding width
		if   key in tinyKeys:   width = 100
		elif key in smallKeys:  width = 140
		elif key in mediumKeys: width = 180 # 160
		elif key in largeKeys:  width = 220 # 240
		elif key in xlargeKeys: width = 260

		# If we can find the key in the lists, compute size from text length
		else:
			width = (len(key) * 40) + 60

		# Append new tuple to the outputs
		outputList.append((key, width))

	# Once we're done, return the text/width list
	return outputList

# end of parseKeysString()



##
## Main program
##

# Open file in and write header
file = open('out.svg', 'w')
writeSvgHeader(file)

# Parse a test input and print the result
keys = parseKeysString('{ctrl|shift|a}')
print(keys)

# Add a group, starting at y = 75, for the keys
file.write('\t<g transform="translate(0, 75)">\n')

# Init variables for the loop below
keyCount = len(keys)
position = 0

# Loop in all key we got above
for i in range(keyCount):
	key = keys[i]

	# Create key
	writeSvgKey(file, key[0], position, key[1])
	position += key[1] + 50

	# If we're not at the en of the array, add a '+' separator
	if i < (keyCount - 1):
		writeSvgPlusSym(file, position)
		position += 50



# # Add some keys
# writeSvgKey(file, 'Ctrl', 100, 160)
# writeSvgPlusSym(file, 390)
# writeSvgKey(file, 'Shift', 440, 240)
# writeSvgPlusSym(file, 650)
# writeSvgKey(file, 'Space', 700, 240)


# end of group
file.write('\t</g>\n')


# Write footer and close the file
writeSvgFooter(file)
file.close()
