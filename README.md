keyComb-gen
===========

keyComb-gen is the short name for **Key**s **Comb**ination **Gen**enerator.

It is a small python script that allows you to render nice SVG images in order
to represent keyboard keys/mouse press combinations for e.g documenting a
software's list of shortcuts.
